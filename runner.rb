require_relative "lib/order_assigner"

orders = [
    {"restaurant_location": "28.504212, 77.044272", "ordered_time": "#{Time.now - 22 * 60}", "id": 321, "name": "home"},
    {"restaurant_location": "28.504288, 77.038264", "ordered_time": "#{Time.now - 2 * 60}", "id": 123, "name": "sector 23"},
    {"restaurant_location": "28.499815, 77.027410", "ordered_time": "#{Time.now - 11 * 60}", "id": 221, "name": "sector 5"}
]
des = [
    {"id":567, "current_location":"28.500576, 77.026817", "last_order_delivered_time": "#{Time.now - 62 * 60}", "name": "main road"},
    {"id":765, "current_location":"28.502167, 77.029057", "last_order_delivered_time": "#{Time.now - 32 * 60}", "name": "krishna chowk"},
    {"id":766, "current_location":"28.503484, 77.033708", "last_order_delivered_time": "#{Time.now - 140 * 60}", "name": "near columbia"},
    {"id":725, "current_location":"28.505419, 77.037749", "last_order_delivered_time": "#{Time.now - 92 * 60}", "name": "secotr 23"},
]

puts OrderAssigner.build(orders, des).run
