require_relative 'order'
require_relative 'delivery_executive'
require_relative 'orders_sorting/time_based'
require_relative 'order_mappers/default'
require_relative 'order_mappers/de_max_utilized'


class OrderAssigner
    attr_accessor :orders, :delivery_executives, :result, :klass_order_mapper,
                  :klass_orders_sorting

    def self.build(orders, delivery_executives,
                    klass_order_mapper = OrderMappers::Default,
                    klass_orders_sorting = OrdersSorting::TimeBased)
        new(Order.build_orders(orders),
            DeliveryExecutive.build_delivery_executives(delivery_executives),
            klass_order_mapper,
            klass_orders_sorting)
    end

    def initialize(orders, delivery_executives, klass_order_mapper, klass_orders_sorting)
        @orders = orders
        @delivery_executives = delivery_executives
        @klass_order_mapper = klass_order_mapper
        @klass_orders_sorting = klass_orders_sorting
        @result = []

    end


    def run
        sort_orders
        assign_orders_to_delivery_executives
        return result
    end

    def sort_orders
        # PriorityBased
        self.orders = klass_orders_sorting.apply(orders)
    end

    def assign_orders_to_delivery_executives
        orders.each do |order|
            result << klass_order_mapper.new(order, delivery_executives).apply
        end
    end
end

