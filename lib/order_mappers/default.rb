require_relative "../utils/geo_distance_calculator"

module OrderMappers
    class Default
        PRIORITY_WEIGHT = {
            distance: 10,
            de_waiting: 1
        }

        MAX_DISTANCE = 10
        MAX_WAITING = 6
        attr_reader :order, :delivery_executives

        def self.priority_weight(key)
            PRIORITY_WEIGHT[key]
        end

        def initialize(order, delivery_executives)
            @order = order
            @delivery_executives = delivery_executives.select(&:is_available)
        end

        def apply
            de = get_de
            de.is_available = false
            return {order_id: order.id, de_id: de.id}
        end

        def get_de
            max_score = 0
            selected_de = nil
            delivery_executives.each do |de|
                distance_factor   = get_distance_factor(order, de)
                de_waiting_factor = get_de_waiting_factor(de)
                score = calculate_score(distance_factor, de_waiting_factor)
                if score > max_score
                    selected_de = de
                    max_score = score
                end
            end
            selected_de
        end

        protected
            def get_distance_factor(order, de)
                # in kms
                distance = GeoDistanceCalculator.get_distance(order.lat, order.long, de.lat, de.long) / 1000.0
                # invert distance to add it to the score in a way that greater value is better
                #
                # if the distance is greater then max thrashold then just return max thrashold as
                # distance value, we'll solve this thing on request validation and cleaning phase
                distance = distance > MAX_DISTANCE ? MAX_DISTANCE : MAX_DISTANCE - distance
                distance / MAX_DISTANCE
            end

            def get_de_waiting_factor(de)
                (de.last_order_delivered_time_in_mins / 60.0) / MAX_WAITING
            end

            def calculate_score(distance_factor, de_waiting_factor)
                # puts [distance_factor, de_waiting_factor,
                #       (distance_factor * self.class.priority_weight(:distance)) +
                #       (de_waiting_factor * self.class.priority_weight(:de_waiting))].to_s
                #
                return (distance_factor * self.class.priority_weight(:distance)) +
                      (de_waiting_factor * self.class.priority_weight(:de_waiting))
            end
    end
end
