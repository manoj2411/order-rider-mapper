require_relative "default"

module OrderMappers
    class DeMaxUtilized < OrderMappers::Default
        PRIORITY_WEIGHT = {
            distance: 1,
            de_waiting: 10
        }

        def self.priority_weight(key)
            PRIORITY_WEIGHT[key]
        end
    end
end
