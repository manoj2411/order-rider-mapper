require 'time'

class Order
    attr_accessor :id, :lat, :long, :ordered_time, :location_label, :prepration_time
    def initialize(id:, lat:, long:, ordered_time:, location_label:)
        self.id             = id
        self.lat            = lat
        self.long           = long
        self.ordered_time   = Time.parse(ordered_time)
        self.location_label = location_label
    end

    # orders = [
    #     {"restaurant_location": "28.504288, 77.038264", "ordered_time":"T1", "id": 123, "name": "sector 23"},
    #     {"restaurant_location": "28.504212, 77.044272", "ordered_time":"T2", "id": 321, "name": "new home"},
    #     {"restaurant_location": "28.499815, 77.027410", "ordered_time":"T3", "id": 321, "name": "sector 5"}
    # ]
    def self.build_orders(attrs_list)
        orders = []
        attrs_list.each do |order_hash|
            lat, lng = order_hash[:restaurant_location].split(',').map{|e| e.strip.to_f}
            order = new(id: order_hash[:id],
                        location_label: order_hash[:name],
                        ordered_time: order_hash[:ordered_time],
                        lat: lat,
                        long: lng)
            orders.push(order)
        end
        return orders
    end
end
