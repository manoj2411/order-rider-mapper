module OrdersSorting
    module TimeBased
        extend self
        def apply(orders)
            orders.sort_by {|e| e.ordered_time }
        end
    end
end
