require 'time'

class DeliveryExecutive
    attr_accessor :id, :lat, :long, :last_order_delivered_time, :is_available, :location_label
    def initialize(id:, last_order_delivered_time:, location_label:, lat:, long:)
        self.is_available              = true
        self.id                        = id
        self.last_order_delivered_time = Time.parse(last_order_delivered_time)
        self.location_label            = location_label
        self.lat                       = lat
        self.long                      = long
    end

    # attrs_list = [
    #     {"id":567, "current_location":"28.500576, 77.026817", "last_order_delivered_time": "2018-02-25 07:55:47 +0530", "name": "main road"},
    #     {"id":765, "current_location":"28.502167, 77.029057", "last_order_delivered_time": "2018-02-25 08:30:47 +0530", "name": "krishna chowk"},
    #     {"id":766, "current_location":"28.503484, 77.033708", "last_order_delivered_time": "2018-02-25 06:35:47 +0530", "name": "near columbia"},
    #     {"id":765, "current_location":"28.505419, 77.037749", "last_order_delivered_time": "2018-02-25 07:35:47 +0530", "name": "secotr - 23"},
    # ]

    def self.build_delivery_executives(attrs_list)
        delivery_executives = []
        attrs_list.each do |attrs_hash|
            lat, lng = attrs_hash[:current_location].split(',').map{|e| e.strip.to_f}
            executive = new(id: attrs_hash[:id],
                            last_order_delivered_time: attrs_hash[:last_order_delivered_time],
                            location_label: attrs_hash[:name],
                            lat: lat,
                            long: lng)
            delivery_executives.push(executive)
        end
        return delivery_executives
    end

    def last_order_delivered_time_in_mins
        ((Time.now - last_order_delivered_time) / 60).round
    end
end
